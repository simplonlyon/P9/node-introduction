import { Router } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";


export const userRouter = Router();

userRouter.get('/', async (req, resp) => {
    const repo = getRepository(User);
    try {
        let users = await repo.find();
        resp.json(users);
    } catch(e) {
        resp.status(500).json(e);
    }
});

userRouter.get('/:id', async (req, resp) => {
    const repo = getRepository(User);
    try {
        let user = await repo.findOne(req.params.id);
        resp.json(user);
    } catch(e) {
        resp.status(500).json(e);
    }
});

userRouter.post('/', async (req, resp) => {
    const repo = getRepository(User);
    try {
        let user = req.body;
        await repo.save(user);
        resp.json(user);
    } catch(e) {
        resp.status(500).json(e);
    }
});

userRouter.delete('/:id', async (req, resp) => {
    const repo = getRepository(User);
    try {
        let user = await repo.findOne(req.params.id);
        await repo.remove(user);
        resp.json(user);
    } catch(e) {
        resp.status(500).json(e);
    }
});


userRouter.patch('/:id', async (req, resp) => {
    const repo = getRepository(User);
    try {
        await repo.update(req.params.id, req.body);
        let user = await repo.findOne(req.params.id);
        resp.json(user);
    } catch(e) {
        resp.status(500).json(e);
    }
});