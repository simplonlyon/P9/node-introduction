import { Router } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/User";


export const first = Router();


let list:string[] = ['ga', 'zo'];

first.get('/', (req, resp) => {
    const repo = getRepository(User);
    
    repo.find().then(users => resp.json(users))
    .catch(err => resp.status(500).json(err));
   
});

first.get('/list', (req, resp) => resp.json(list));

first.post('/list', (req, resp) => {
    if(!req.body.item) {
        return resp.status(400).json({msg: 'You should have an item property'});
    }
    
    list.push(req.body.item);
    resp.status(201).json({msg: 'item added'});

});