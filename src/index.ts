import * as express from 'express';
import bodyParser = require('body-parser');
import * as cors from 'cors';
import { first } from './router/first-router';
import { createConnection } from 'typeorm';
import { userRouter } from './router/user-router';

createConnection('default');

const app = express();

app.use(cors());
app.use(bodyParser.json());


app.use('/first', first);
app.use('/user', userRouter);



app.listen(3000, () => console.log('app listening on 3000'));

